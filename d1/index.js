/*
	on d1 folder

	gitbash

	npm init
	
	npm install express

	touch .gitignore

*/

/*

 - A "module" is a software component or part of a program that contains one or more routines

 -This is used to get contents of the package to be used by our application

 -This is used to get the contents of the package to be used by our application

 -It also allows us to access methods and functions that will allow us to easily create a server

*/

const express = require("express");

/*
	
	- Create an application using express
	- This creates an express application and stores this in a constant called app
	- In layman's term, app is our server

*/

const app = express()

// For our application server to run, we need a port to listen to
const port = 3000;

// MIDDLEWARES

/*
-Set up for allowing the server to handle data from requests
-Allows your app to read json data
-Methods used from express.js are middlewares
-Middleware is a layer of software that enables interaction and transmission of information between assorted applications
*/

app.use(express.json());

/*

-Allows your app to read data from forms
-By default, information received from the URL can only be received as a string or an array
-By applying the option of "extended:true", we are allowed to receive information in other data types as an object througout our application
*/

app.use(express.urlencoded({extended:true}));

// ROUTES

/*
	-Express has methods corresponding to each HTTP method
	-The full base URL for our local application for this route will be at "http://localhost:3000"

*/

// RETURNS SIMPLE MESSAGE

/*
	-This route expects to receive a GET request at the base URI

	POSTMAN:
	url: http://localhost:3000/
	method: GET

*/

app.get('/', (request, response) => {
	response.send("Hello World")
})



// RETURNS SIMPLE MESSAGE 2

/*

	POSTMAN:

	URI: /hello
	Method: 'GET'


	url: http://localhost:3000/hello
	method: GET

*/

app.get('/hello', (request, response) => {
	response.send("Hello from the '/hello' endpoint")
})


// RETURNS SIMPLE MESSAGE 2

/*

	POSTMAN:

	URI: /hello
	Method: 'POST'


	url: http://localhost:3000/hello
	method: POST
	body: raw + json
		{
			"firstName": "Paul",
			"lastName": "Santos"
		}

*/

app.post('/hello', (request, response) => {
	response.send(`Hello there, ${request.body.firstName} ${request.body.lastName}! This is from the "/hello" endpoint but with a post method`)
})


let users = [];

// REGISTER USER ROUTE

/*
	- This route expects to receive a POST request at the URI "/register"
	-This will create a user object in the "users" variable that mirrors a real world registration process

	URI: /hello
	Method: 'POST'

	POSTMAN
	url: http://localhost:3000/register
	method: POST
	body: raw + json
		{
			"username": "Paul",
			"password": " "
		}

*/

app.post('/register', (request, response) => {
	if (request.body.username !== '' && request.body.password !== '') {
		users.push(request.body);
		response.send(`User ${request.body.username} successfully registered!`)
	} else {
		response.send('Please input BOTH username and password')
	}
})

// CHANGE PASSWORD ROUTE

/*
	- This route expects to receive a PUT request at the URI "/change-password"
	- This will update the password of a user that matches the information provided in the client/postman

	URI: /change-password
	Method: 'PUT'

	POSTMAN
	url: http://localhost:3000/change-password
	method: PUT
	body: raw + json
		{
			"username": "Paul",
			"password": "paul123 "
		}

*/


app.put('/change-password', (request, response) => {
	// Creates a variable to store the message to be sent back to the client/postman
	let message;

	// Creates a for loop that will loop through the elements of the "users" array
	for (let i = 0; i < users.length; i++) {
		// if the username provided in the client/postman and the username of the current object in the loop is the same
		if (request.body.username == users[i].username) {
			// Changes the password of the user found by the loop into the password provided in the client/Postman
			 users[i].password = request.body.password
			 // Changes the message to be sent if the password has been updated
			 message = `User ${request.body.username}'s password has been updated`
			 // Breaks out the loop once a user that matches the username provided in the client/Postman is found
			 break;
		 // If no user is found, else 
		} else {
			// Changes the mesage to be sent back by the response
			message = "Users does not exist"
		}
	}

	response.send(message);
})

/*
	-Tells our server to listen to the port
	-If the port is accessed, we can run the server
	-Returns a message to confirm that the server is running in the terminal

*/

app.listen(port, () => console.log(`Server running at port ${port}`));

