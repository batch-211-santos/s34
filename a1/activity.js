
const express = require("express");

const app = express();

const port = 3000;


app.use(express.json());

app.use(express.urlencoded({extended:true}));


let users = [
    {
        "username" : 'johndoe', 
        "password" : 'johndoe123'
    }
 	]

// 1

app.get('/home', (request, response) => {
	response.send("Welcome to the home page")
})



// 1.5

app.post('/home', (request, response) => {
	response.send("Welcome to the home page")
})


app.post('/signup', (request, response) => {
	if (request.body.username !== '' && request.body.password !== '') {
		users.push(request.body);
		response.send(`User ${request.body.username} successfully registered!`)
	} else {
		response.send('Please input BOTH username and password')
	}
})

/*
		{
			"username": "Paul",
			"password": "paul321"
		}

*/


// 2

app.get('/users', (request, response) => {
	response.send(users)
})

// 3

app.delete('/delete-users', (request, response) => {
	users = [];
	response.send(`Users has been deleted!`);
	// response.send(users)
})



app.listen(port, () => console.log(`Server running at port ${port}`));